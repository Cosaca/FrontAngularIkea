# BITBOX IKEA TEST FRONT

## Dependencies

Run `npm i` to download the dependencies the project needs. 


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
