import { Product } from '../models/Product';
import { ProductRequest } from '../models/Request/ProductRequest';
import { PriceReduction } from '../models/PriceReduction';

export module ProductConverter {
    export function convertProductToProductRequest(product: Product): ProductRequest {
        let productRequest = new ProductRequest();
        productRequest.code = product.code;
        productRequest.creationDate = product.creationDate;
        productRequest.description = product.description;
        productRequest.idUserCreator = product.userCreator;
        productRequest.price = product.price;

        if(product.priceReductions) {
            product.priceReductions.forEach(priceRed => {
                if(productRequest.priceReductions == null) {productRequest.priceReductions = []}
                productRequest.priceReductions.push(priceRed);
            });
        }

        if(product.supplierList) {
            product.supplierList.forEach(supplier => {
                productRequest.supplierList.push(supplier);
            });
        }

        productRequest.state = product.state;

        return productRequest;
    }
}