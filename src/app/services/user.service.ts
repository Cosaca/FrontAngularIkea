import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/User';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  
  baseUrl = "http://localhost:8080/private/";

  constructor(private http: HttpClient, private loginService: LoginService) { }

  public getAllUsers(): Promise<User[]> {
    return this.http.get(this.baseUrl + "users/getUsers?access_token="+this.loginService.getTokenUser().access_token).toPromise() as Promise<User[]>;
  }

  public getUsersById(ids: string[]): Promise<User[]> {
    
    return this.http.get(this.baseUrl + "users/getUsersById?access_token="+this.loginService.getTokenUser().access_token+"&id=" + this.parseIds(ids)).toPromise() as Promise<User[]>;
  }

  private parseIds(ids : string[]): string {
    let idparsed = "";
    for (let i = 0; i < ids.length-1; i++) {
      idparsed = idparsed.concat(ids[i]+",");
    }
    idparsed = idparsed.concat(ids[ids.length-1].toString());

    return idparsed;
  }
}
