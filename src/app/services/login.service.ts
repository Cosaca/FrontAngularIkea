import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Token } from '../models/Token';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUrl = "http://localhost:8080/private/";

  constructor(private http: HttpClient) { }

  doLogin(username:string, password: string): Promise<Token> {

     
    let httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'Authorization': 'Basic bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0'
         });    
         let options = {
      headers: httpHeaders
         };       
    

    return this.http.post("http://localhost:8080/oauth/token?grant_type=password&username="+username+"&password="+password, null, options).toPromise() as Promise<Token>;
  }

  getUserLoggedIn() {
    return this.http.get(this.baseUrl + "users/currentUser?access_token="+this.getTokenUser().access_token).toPromise();
  }

  setUserLoggedIn(user) {
    localStorage.setItem("user", JSON.stringify(user));
  }

  setToken(token){
    localStorage.setItem("token", JSON.stringify(token));
  }

  getUserLoggedInFromLocalStorage(){
    return JSON.parse(localStorage.getItem("user"));
  }

  getTokenUser(): Token {
    return JSON.parse(localStorage.getItem("token")) as Token;
  }

  shutDown() {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
  }


}
