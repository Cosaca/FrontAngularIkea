import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Supplier } from '../models/Supplier';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  baseUrl = "http://localhost:8080/private/";

  constructor(private http: HttpClient, private loginService:LoginService) { }

  public getAllSuppliers() : Promise<Supplier[]> {
    return this.http.get(this.baseUrl + "suppliers/getAll?access_token="+this.loginService.getTokenUser().access_token).toPromise() as Promise<Supplier[]>;
  }

}
