import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/Product';
import { Observable } from 'rxjs';
import { ProductRequest } from '../models/Request/ProductRequest';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  
  baseUrl = "http://localhost:8080/private/";

  constructor(private http: HttpClient, private loginService: LoginService) { }

  public getAllProducts() : Promise<Product[]> {
    return this.http.get(this.baseUrl + "products/getAll?access_token="+this.loginService.getTokenUser().access_token).toPromise() as Promise<Product[]>;
  }

  public updateProduct(product: ProductRequest) : Promise<Product> {
    return this.http.post(this.baseUrl + "products/updateProduct?access_token="+this.loginService.getTokenUser().access_token, product).toPromise() as Promise<Product>;
  }

  public getProductById(id: number): Promise<Product> {
    return this.http.get(this.baseUrl + "products/getProductById?access_token="+this.loginService.getTokenUser().access_token+"&id="+id ).toPromise() as Promise<Product>;
  }

  public addProduct(product: ProductRequest) : Promise<Product> {
    return this.http.post(this.baseUrl + "products/addProduct?access_token="+this.loginService.getTokenUser().access_token, product).toPromise() as Promise<Product>;
  }
}
