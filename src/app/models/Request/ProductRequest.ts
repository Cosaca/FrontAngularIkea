import { PriceReduction } from '../PriceReduction';
import { Supplier } from '../Supplier';

export class ProductRequest {
    public code: number;
	public description: String;
	public price: number;
	public state: boolean;
	public creationDate: Date;
	public idUserCreator: string;
	public supplierList: Supplier[] = [];
	public priceReductions: PriceReduction[] = [];
}