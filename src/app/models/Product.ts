import { User } from './User';
import { Supplier } from './Supplier';
import { PriceReduction } from './PriceReduction';

export class Product {
    public code: number;
	public description: string;
	public price: number;
	public state : boolean = true;
	public creationDate: Date ;
	public userCreator: string;
	public supplierList: Supplier[] = [];
	public priceReductions: PriceReduction[] = [];
}