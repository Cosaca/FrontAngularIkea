import { Product } from './Product';

export class User {
	
	public userName: string;
	
	public surname: string;
	
	public password: string;

	public dni: String;

	public products: Product [];

}