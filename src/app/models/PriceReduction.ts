export class PriceReduction {
	public id: number;
	public reducedPrice: number;

	public startDate: Date;

	public endDate: Date;
}