import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/models/Product';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';
import { Supplier } from 'src/app/models/Supplier';
import { SupplierService } from 'src/app/services/supplier.service';
import { PriceReduction } from 'src/app/models/PriceReduction';
import { ProductConverter } from 'src/app/converters/ProductConverter';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  constructor( private route: ActivatedRoute,
    private router: Router,
    private _snackBar: MatSnackBar,
    private userService: UserService,
    private supplierService: SupplierService,
    private productService: ProductService) { }

    productId: string;
    product: Product;
    users: User[];
    supplierList: Supplier[];
    selectedSupplier: Supplier;
    selectedUser: User;
    newCreationDate: Date;
    newDiscount: number;
    startDate: Date;
    endDate: Date;  

    isNewProduct = false;


  ngOnInit(): void {
    if(this.route.snapshot.paramMap.get("id") != null) {
      this.productId = this.route.snapshot.paramMap.get("id")
      this.load();
    } else {
      this.product = new Product();
      this.isNewProduct = true;
      this.loadUsers();
      this.loadSuppliers();
    }
  }

  async load() {
    
    this.product = await this.productService.getProductById(Number.parseInt(this.productId));
    await this.loadUsers();

    await this.loadSuppliers();
  }

  private async loadSuppliers() {
    this.supplierList = await this.supplierService.getAllSuppliers();
  }

  private async loadUsers() {
    this.users = await this.userService.getAllUsers();
    
    this.users.forEach(user => {
      if (user.userName == this.product.userCreator) {
        this.selectedUser = user;
        return;
      }
    });
  }

  addSupplier() {
    let repetido = false;
    this.product.supplierList.forEach(putoSupplier => {
      if(putoSupplier.id == this.selectedSupplier.id) {
        repetido = true;
      }
    })
    
    if(!repetido) {
      this.product.supplierList.push(this.selectedSupplier);
    }

    
  }
  addPriceReduction() {
    if(this.newDiscount != null && this.startDate != null) {

      let priceReduction = new PriceReduction();
      priceReduction.reducedPrice = this.newDiscount;
      priceReduction.startDate = this.startDate;
      priceReduction.endDate = this.endDate;
  
  
      this.product.priceReductions.push(priceReduction);
    }
    
  }

  generateProduct(): Product {
    let producToUpdate  = new Product();
    producToUpdate.code = this.product.code;
    if(this.newCreationDate != null) {
      producToUpdate.creationDate = this.newCreationDate;
    }else {
      producToUpdate.creationDate = this.product.creationDate;
    }
    producToUpdate.userCreator = this.selectedUser.userName;
    producToUpdate.description = this.product.description;
    producToUpdate.price = this.product.price;
    producToUpdate.priceReductions = this.product.priceReductions;
    producToUpdate.state = this.product.state;
    producToUpdate.supplierList = this.product.supplierList;

    return producToUpdate;
  }

  async updateProduct() {  
   let productToUpdate = this.generateProduct();
    
   if(!this.isNewProduct) {
    this.product = await this.productService.updateProduct(ProductConverter.convertProductToProductRequest(productToUpdate));
    this.openSnackBar("Updated Successfully!");
   } else {
    this.product = await this.productService.addProduct((ProductConverter.convertProductToProductRequest(productToUpdate)));
    this.openSnackBar("Created Successfully!");
    this.goBack();
   }
  }

  removePriceReduction(reduction:PriceReduction) {
    this.product.priceReductions.splice((this.product.priceReductions.indexOf(reduction)), 1)
  }

  removeTransportist(supplier: Supplier) {
    this.product.supplierList.splice((this.product.supplierList.indexOf(supplier)), 1)
  }

  getEstado(estado: boolean): string {
    return estado ? "Active" : "Discontinued"; 
  }

  async changeState(product: Product) {
    product.state = !product.state;

  }

  openSnackBar(message: string) {

    const config = new MatSnackBarConfig();
    config.panelClass = ['custom-class'];
    config.duration = 5000,
    config.horizontalPosition = "end";
    config.verticalPosition = "bottom";


    this._snackBar.open(message, null, config);
  }

  goBack() {
    this.router.navigateByUrl("/home");
  }

}
