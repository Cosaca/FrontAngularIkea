import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  userLogged: string;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.userLogged = this.loginService.getUserLoggedInFromLocalStorage().username;
    console.log(this.loginService.getUserLoggedInFromLocalStorage());
  }

  logout() {
    this.loginService.shutDown();
    this.router.navigateByUrl("/login")
  }

}
