import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { ThrowStmt } from '@angular/compiler';
import { error } from 'protractor';
import { Router } from '@angular/router';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {


  username: string;
  password: string;
  mostrarErrorLogin = false;

  constructor(private loginService: LoginService, private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

   onLogin() {
     this.loginService.doLogin(this.username, this.password).then(
      result => {
        this.loginService.setToken(result);
        this.getUserLoggedIn();
        this.mostrarErrorLogin = false;
      }
    ).catch(
      error => {
        this.mostrarErrorLogin = true;
        this.openSnackBar('Credenciales incorrectas');
      }
    );
  }

  async getUserLoggedIn() {
    await this.loginService.getUserLoggedIn().then(
      res => {
       this.loginService.setUserLoggedIn(res);
        this.router.navigateByUrl("/home")
      }
    ).catch(err => {
      console.log(err);
    })
  }

  openSnackBar(message: string) {

    const config = new MatSnackBarConfig();
    config.panelClass = ['custom-class'];
    config.duration = 2500,
    config.horizontalPosition = "center";
    config.verticalPosition = "bottom";


    this._snackBar.open(message, null, config);
  }

}
