import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/models/Product';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/User';
import { MatTableDataSource } from '@angular/material/table';
import { ProductConverter } from 'src/app/converters/ProductConverter';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  displayedColumns: string[] = ['code', 'description' , 'price', 'state', 'creationDate', 'userCreator', 'acciones'];
  dataSource = new MatTableDataSource();

  productos: Product[];
  users: User[];

  usersId: string[] = [];
  estaMostrandoDivFiltros = false;

  constructor(private productService: ProductService, private userService: UserService) { }

  ngOnInit(): void {
    this.load();
  }

  async load() {
    this.productos = await this.productService.getAllProducts();

    this.productos.forEach(producto => {
      this.usersId.push(producto.userCreator);
    })
    
    
    this.users = await this.userService.getUsersById(this.usersId);

    this.dataSource.data = this.productos;
  }

  getEstado(estado: boolean): string {
    return estado ? "Active" : "Discontinued"; 
  }

  getUserCreatorName(username: string): User {
    let userCreator: User = null;
    this.users.forEach(user => {
      if(user.userName == username) {
        userCreator = user;
      }
    })
    return userCreator;
  }

  applyFilter(event) {
    if(event==null) {
      this.dataSource.filter = "";
      return;
    }
    const filterValue = event;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  mostrarFiltros() {
    this.estaMostrandoDivFiltros = !this.estaMostrandoDivFiltros;
  }

  async changeState(product: Product) {
    let productToUpdate = Object.create(product);
    productToUpdate.state = !productToUpdate.state;
    let prodUpdated = await this.productService.updateProduct(ProductConverter.convertProductToProductRequest(productToUpdate));
    product.state = prodUpdated.state;

  }
}
